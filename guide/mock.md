# 数据&联调

## 如何使用

::: tip 说明：

- 建议在根目录下新建/common/http.interceptor.js 文件，也即创建 common 目录(如果没有的话)，再创建 http.interceptor.js 文件，将拦截器相关代码写在里面。
- 请求 loading 超时时间的意义为，一般情况下，请求会在几十毫秒返回，时间极短，无需 loading，如果显示 loading，会导致 动画一闪而过，体验不好。如果用户网络慢，或者服务器堵塞，可能一个请求需要几秒钟，这时请求达到设定时间(800ms)， 就会显示 loading，几秒钟后请求返回，loading 消失。
- 何谓请求拦截？顾名思义，就是在请求发出之前，对请求做一些额外处理，比如对不同 api 接口，携带不同的 header 参数，或者(也是最重要) 配置统一的 token 到 header 中。
- 何谓响应拦截？响应拦截，意味着是在请求返回时，对返回的数据进行一些处理，如不同的状态对应的关系，比如约定状态码 200 为成功。

:::

如果您是微信小程序，请配置您的接口域名并且使用 https 方式请求，否则上线后无法使用

### 配置

修改您接口前缀

```bash
# 替换成您的接口前缀
const baseUrl = 'https://sourcebyte.vip';
```

### 完整案例

```js
// /common/http.interceptor.js

// 这里的vm，就是我们在vue文件里面的this，所以我们能在这里获取vuex的变量，比如存放在里面的token变量
const install = (Vue, vm) => {
  // 此为自定义配置参数，具体参数见上方说明
  Vue.prototype.$u.http.setConfig({
    baseUrl: 'https://api.example.com',
    loadingText: '努力加载中~',
    loadingTime: 800,
    // ......
  });

  // 请求拦截，配置Token等参数
  Vue.prototype.$u.http.interceptor.request = (config) => {
    // 引用token
    // 方式一，存放在vuex的token，假设使用了uView封装的vuex方式
    // 见：https://uviewui.com/components/globalVariable.html
    // config.header.token = vm.token;

    // 方式二，如果没有使用uView封装的vuex方法，那么需要使用$store.state获取
    // config.header.token = vm.$store.state.token;

    // 方式三，如果token放在了globalData，通过getApp().globalData获取
    // config.header.token = getApp().globalData.username;

    // 方式四，如果token放在了Storage本地存储中，拦截是每次请求都执行的
    // 所以哪怕您重新登录修改了Storage，下一次的请求将会是最新值
    // const token = uni.getStorageSync('token');
    // config.header.token = token;
    config.header.Token = 'xxxxxx';

    // 可以对某个url进行特别处理，此url参数为this.$u.get(url)中的url值
    if (config.url == '/user/login') config.header.noToken = true;
    // 最后需要将config进行return
    return config;
    // 如果return一个false值，则会取消本次请求
    // if(config.url == '/user/rest') return false; // 取消某次请求
  };

  // 响应拦截，判断状态码是否通过
  Vue.prototype.$u.http.interceptor.response = (res) => {
    if (res.code == 200) {
      // res为服务端返回值，可能有code，result等字段
      // 这里对res.result进行返回，将会在this.$u.post(url).then(res => {})的then回调中的res的到
      // 如果配置了originalData为true，请留意这里的返回值
      return res.result;
    } else if (res.code == 201) {
      // 假设201为token失效，这里跳转登录
      vm.$u.toast('验证失败，请重新登录');
      setTimeout(() => {
        // 此为uView的方法，详见路由相关文档
        vm.$u.route('/pages/user/login');
      }, 1500);
      return false;
    } else {
      // 如果返回false，则会调用Promise的reject回调，
      // 并将进入this.$u.post(url).then().catch(res=>{})的catch回调中，res为服务端的返回值
      return false;
    }
  };
};

export default {
  install,
};
```

### 使用案例

```js
// login.vue
export default {
  methods: {
    // post示例
    sumbitByPost() {
      this.$u
        .post('/user/login', {
          username: 'lisa',
          password: '123456',
        })
        .then((res) => {
          // res为服务端返回的数据
        });
    },

    // get示例
    sumbitByGet() {
      this.$u
        .get('/user/login', {
          username: 'lisa',
          password: '123456',
        })
        .then((res) => {
          // res为服务端返回的数据
        });
    },
  },
};
```

::: tip 注意

**`此token是登录成功后后台返回保存在storage中的`**

**`Token请求拦截`**，您可随意配置跳转到您项目的任何页面或弹窗提醒。

:::

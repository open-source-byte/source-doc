# 基础配置

用于修改项目的依赖、权限、配色、布局、路由、组件默认配置

- 项目的基本配置位于项目根目录下的 `manifest.json`
- `manifest.json` 文件是应用的配置文件，用于指定应用的名称、图标、权限等。HBuilderX 创建的工程此文件在根目录，CLI 创建的工程此文件在 src 目录。

- 具体可以参考 [manifest 文档](https://uniapp.dcloud.io/collocation/manifest)

::: warning 注意

\*文件内 `基础配置` 中 `uni-app应用标识(AppId)`, 是唯一的，是需要您重新获取

\*文件内选择 `微信小程序配置` 中 `AppID` 需要您申请小程序填写您的 AppID，否则无法 `真机预览`、`打包`

:::

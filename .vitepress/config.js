// @ts-check
/**
 * @type {import('vitepress').UserConfig}
 */
module.exports = {
  lang: 'zh-CN',
  copyright: '杭州源字节科技有限公司版权所有',
  title: '开源字节',
  keywords:
    '软件开发公司,软件开发,软件定制,软件外包,小程序开发,小程序定制,小程序定制开发,app开发,app定制,网站开发,网站建设,项目管理软件,工程管理软件,企业信息化,源字节科技,杭州源字节科技有限公司,开源字节,开源软件,JAVA,UNIAPP,快速开发平台',
  description: '我们追求极佳的产品体验和服务感受,推陈出新,不断突破自我寻求更高的层次',
  head: createHead(),
  themeConfig: {
    repo: 'https://gitee.com/open-source-byte/source-vue',
    repoLabel: '开源字节',
    docsRepo: 'https://gitee.com/open-source-byte/source-vue',
    logo: '/logo.png',
    docsBranch: 'doc',
    editLinks: false,
    editLinkText: '为此页提供修改建议',
    nav: createNav(),
    sidebar: createSidebar(),
  },
};

/**
 * @type {()=>import('vitepress').HeadConfig[]}
 */

function createHead() {
  return [
    ['meta', { name: 'author', content: '开源字节' }],
    [
      'meta',
      {
        name: 'keywords',
        content:
          '软件开发公司,软件开发,软件定制,软件外包,小程序开发,小程序定制,小程序定制开发,app开发,app定制,网站开发,网站建设,项目管理软件,工程管理软件,企业信息化,源字节科技,杭州源字节科技有限公司,开源字节,开源软件,JAVA,UNIAPP,快速开发平台',
      },
    ],
    ['link', { rel: 'icon', type: 'image/svg+xml', href: '/logo.svg' }],
    [
      'meta',
      {
        name: 'viewport',
        content:
          'width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no',
      },
    ],
    ['link', { rel: 'icon', href: '/favicon.ico' }],
  ];
}

/**
 * @type {()=>import('./theme-default/config').DefaultTheme.NavItem[]}
 */
function createNav() {
  return [
    {
      text: '首页',
      link: '/',
    },
    {
      text: '指南',
      link: '/guide/introduction',
    },
    {
      text: '相关链接',
      items: [
        {
          text: '在线体验',
          link: 'http://boot.sourcebyte.vip',
        },
        {
          text: '项目源码',
          link: 'https://gitee.com/open-source-byte/source-vue',
        },
        {
          text: '文档源码',
          link: 'https://gitee.com/open-source-byte/source-doc',
        },
        {
          text: '点赞支持',
          link: 'https://gitee.com/open-source-byte/source-vue/stargazers',
        },
      ],
    },
    {
      text: '白皮书',
      link: 'https://sourcebyte.vip/profile/customer/file/%E5%BC%80%E6%BA%90%E5%AD%97%E8%8A%82%E4%BA%A7%E5%93%81%E7%99%BD%E7%9A%AE%E4%B9%A6.pdf',
    },
    {
      text: '关于',
      link: 'https://sourcebyte.vip/about',
    },
  ];
}

function createSidebar() {
  return {
    '/': [
      {
        text: '介绍',
        link: '/guide/introduction',
      },
      {
        text: '开始',
        children: [
          {
            text: '环境部署',
            link: '/guide/deploy',
          },
          {
            text: '项目扩展',
            link: '/guide/project',
          },
        ],
      },
      {
        text: '基础',
        children: [
          {
            text: '路由',
            link: '/guide/router',
          },
          {
            text: '数据&联调',
            link: '/guide/mock',
          },
          {
            text: '全局样式',
            link: '/guide/design',
          },
          {
            text: '基础配置',
            link: '/guide/settings',
          },
        ],
      },
      {
        text: '深入',
        children: [
          {
            text: '跨域处理',
            link: '/dep/cors',
          },
          {
            text: '图标',
            link: '/dep/icon',
          },
          {
            text: '国际化',
            link: '/dep/i18n',
          },
          {
            text: '项目规范',
            link: '/dep/lint',
          },
        ],
      },
      {
        text: '其他',
        children: [
          {
            text: '常见问题',
            link: '/other/see',
          },
          {
            text: '捐赠支持',
            link: '/other/donate',
          },
        ],
      },
    ],
  };
}

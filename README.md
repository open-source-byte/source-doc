# 开源字节-文档

### 说明

开源字节·在线文档，采用 Vitepress 开发。如发现文档有误，欢迎提 pr 帮助我们改进

### 在线体验

演示地址：http://boot.sourcebyte.vip

开发文档：http://doc.sourcebyte.vip

官网地址：https://sourcebyte.vip

## 如何本地开发

```bash
# 克隆本仓库(node 18)
$ git clone https://gitee.com/open-source-byte/source-doc.git

# 安装依赖
$ npm install

# 启动开发服务器
$ npm run dev

# 线上部署
$ npm run build
```
